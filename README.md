README
======
This is the README file from the PARSEME verbal multiword expressions (VMWEs) corpus for Serbian. 
The current version is compatible with the PARSEME annotation [guidelines version 1.3](https://parsemefr.lis-lab.fr/parseme-st-guidelines/1.3/).

Corpora
-------
All annotated data comes from the [Corpus of Contemporary Serbian](http://www.korpus.matf.bg.ac.rs/). Texts belong to daily newspapers and periodicals. They contain 3586 sentences.


Provided annotations
--------------------
The data are in the [.cupt](http://multiword.sourceforge.net/cupt-format) format. Here is detailed information about some columns:

* LEMMA (column 3): Available. Automatically annotated.
* POS (column 4): Available. Automatically annotated.
* FEATS (column 6): Available. Automatically annotated.
* HEAD and DEPREL (columns 7 and 8): Available. Automatically annotated.
* MISC (column 10): No-space information available. Automatically annotated.
* PARSEME:MWE (column 11): Manually annotated. The following [VMWE categories](https://parsemefr.lis-lab.fr/parseme-st-guidelines/1.3/?page=030_Categories_of_VMWEs) are annotated: VID, LVC.full, LVC.cause, IRV.

All automatic annotations in columns LEMMA, POS, FEATS, HEAD and DEPREL were performed with [UDPipe 2](https://ufal.mff.cuni.cz/udpipe/2) using the serbian-set-ud-2.10-220711 model.
The manual VMWEs annotations in the PARSEME:MWE column were performed by the single annotator. 

Statistics
-------
To know the number of annotated VMWEs of different types and with different properties (length, continuity, etc.), use these scripts: [mwe-stats.py](https://gitlab.com/parseme/utilities/-/blob/master/st-organizers/corpus-statistics/mwe-stats.py) and [mwe-stats-simple.py](https://gitlab.com/parseme/utilities/-/blob/master/st-organizers/corpus-statistics/mwe-stats-simple.py). 

Corpus preprocessing
------------
* Tokenization and sentence segmentation was done using the [Unitex corpus processing suite](https://unitexgramlab.org/).
* Lemmatization was performed using [SrpKor4Tagging-TreeTagger](https://live.european-language-grid.eu/catalogue/ld/9296/) 

License
-------
The full dataset is licensed under **Creative Commons Non-Commercial Share-Alike 4.0** license [CC-BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/)

Authors
-------
All VMWEs annotations were performed by Cvetana Krstev. File preparation and transformations were performed by Ranka Stanković.

Future work
-------
* To estimate the quality of the annotations, a subset of the corpus should be double-annotated and inter-annotator agreement shoudl be calculated.
* Annotation of new texts is planned.

Change log
----------
- **2023-04-15**:
  - Version 1.3 of the corpus was released on LINDAT.
  - This is the first publication of the corpus. 
  
Contact
-------
* cvetana@matf.bg.ac.rs
* ranka@rgf.rs
